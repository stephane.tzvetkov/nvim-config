require('plugins')                  -- ✅
require('theme')                    -- ✅
require('indentation')              -- ✅
require('lsp')                      -- ✅
require('completion')               -- ✅
require('spellchecker')             -- ✅
require('snippets')
require('mapping')
require('syntax')                   -- TODO
-- require('formatter')
-- require('fsexplorer')
-- require('fuzzyfinder')
-- require('webbrowser')
require('git')

-- require('ccpp')
-- require('clojure')
-- require('csv')
require('epics_nvim')
-- require('hexa')
-- require('htmlcss')
-- require('java')
-- require('javascript')
-- require('json')
-- require('latex')
require('lua')
-- require('markdown')
require('python')
-- require('shell')
-- require('r')
-- require('rust')
-- require('scala')
-- require('svg')
-- require('toml')
-- require('viml')
-- require('wiki')
-- require('xml')
-- require('yaml')

-- require('misc')


-- TODO
-- Terminal integration
-- Register
-- Marks
-- Note Taking
-- Utility
-- Icon
-- Debugging
-- Neovim Lua Development
-- Fennel
-- Startup
-- Indent
-- Game
-- File explorer
-- Dependency management
-- Programming languages support
-- Comment
-- Collaborative Editing
-- Quickfix
-- Motion
-- Code Runner
-- GitHub
-- Search
-- Scrollbar
-- Scrolling
-- Mouse
-- Project
-- Browser integration
-- Editing support
-- Formatting
-- Web development
-- Media
-- Discord Rich Presence
-- Command Line
-- Session
-- Test
-- Competitive Programming
-- Preconfigured Configuration
-- Keybinding
-- Tmux
-- Remote Development
-- Split and Window

