
## How to install

Here is a little installation procedure that I suggest you to follow in order to try my `nvim` setup.

**Prerequisite**:
- Neovim v0.5.0+ is needed, you can install the [`neovim`
  package](https://repology.org/project/neovim/versions) with your favorite package manager, e.g.:
    - `sudo apk add neovim`
    - `sudo apt install neovim`
    - `sudo emerge -a neovim`
    - `sudo dnf install neovim`
    - `sudo nix-env -iA nixos.neovim` # or `sudo nix-env -iA nixpkgs.neovim` on non-NixOS distro
    - `sudo pacman -S neovim`
    - `sudo xbps-install -S neovim`
    - `sudo yum install neovim`

**Installation**:
- Retrieve my `lua` folder and my `init.lua` file, and put them into your
  `${XDG_CONFIG_HOME:-${HOME}/.config}/nvim` (or wherever is located your neovim configuration
  folder) directory. Either by copying them "manually" from this repository or by running the
  following terminal commands:
    ```console
    $ cd ${XDG_CONFIG_HOME:-${HOME}/.config}
    $ mv nvim pre_nvim # if a pre-existing nvim config is present
    $ git clone https://gitlab.com/stephane.tzvetkov/nvim-config.git nvim
    ```
- Install/sync my config:
    ```console
    $ nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync' 2>&1 /dev/null
    ```
- Done. Enjoy my `nvim` setup!

## References

- <https://github.com/rockerBOO/awesome-neovim>
- <https://github.com/nanotee/nvim-lua-guide>
- <https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/>

## Tests notes

Before running the installation method (in order to test it):
- remove the `$HOME/.local/share/nvim/site` folder, if any
- remove the `$HOME/.config/nvim` folder, if any

