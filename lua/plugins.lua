-- plugins
----------

-- plugins management done with [packer](https://github.com/wbthomason/packer.nvim)

-- see
-- https://github.com/rockerBOO/awesome-neovim#plugin-manager

---------------------------------------------------------------------------------------------------
-- tips
--------------

-- :PackerInstall -> install plugins if they are not already installed
-- :PackerUpdate  -> update plugins, installing any that are missing
-- :PackerClean   -> remove any disabled or no longer managed plugins
-- :PackerSync    -> perform a clean followed by an update
-- :PackerCompile -> compile lazy-loader code and save to path

-- :checkhealth plugin-name -> ensure plugin-name is setup correctly

---------------------------------------------------------------------------------------------------
-- automated packer
-------------------

-- automatically run `:PackerCompile` whenever `plugins.lua` (this file) is updated thanks to this
-- autocommand
-- see https://github.com/wbthomason/packer.nvim#bootstrapping
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

-- automatically install and set up `packer.nvim` on any machine cloning this configuration file
-- see https://github.com/wbthomason/packer.nvim#bootstrapping
local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim' -- e.g. /home/user/.local/share/nvim/site/pack/packer/start/packer.nvim
if fn.empty(fn.glob(install_path)) > 0 then
    packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

---------------------------------------------------------------------------------------------------
-- plugins list
---------------

return require('packer').startup(function(use)

    use 'wbthomason/packer.nvim' -- let packer.nvim manage itself

    ----------------
    -- for ./lsp.lua:
    use 'neovim/nvim-lspconfig'
    -- TO CHECK:
    -- https://github.com/rockerBOO/awesome-neovim#lsp

    -----------------------
    -- for ./completion.lua
    use 'hrsh7th/nvim-cmp'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-nvim-lua'
    use 'hrsh7th/cmp-cmdline'
    -- TO CHECK
    -- https://github.com/topics/nvim-cmp

    -----------------------
    -- for ./snippets.lua
    use 'L3MON4D3/LuaSnip'

    -----------------------
    -- for ./syntax.lua
    use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
    use 'RRethy/vim-illuminate'

    ---------------------
    -- for `./theme.lua`:
    use 'marko-cerovac/material.nvim'
    -- use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'} -- already required by ./syntax.lua
    use {'nvim-lualine/lualine.nvim', requires = {'kyazdani42/nvim-web-devicons', opt = true}}
    use 'norcalli/nvim-colorizer.lua'

    -------------------
    -- for `./git.lua`:
    use {'tanvirtin/vgit.nvim', requires = {'nvim-lua/plenary.nvim'}}

    ---------------------------
    -- for `./fuzzyfinder.lua`:
    use { 'nvim-telescope/telescope.nvim', requires = {{'nvim-lua/plenary.nvim'}}}

    ---------------------------
    -- for `./spellchecker.lua`:
    use {
    -- 'nvim-treesitter/nvim-treesitter', -- optional but recommended to easily install tree-sitter parsers
    'lewis6991/spellsitter.nvim',
    }

    ---------------------------
    -- for `./epics_nvim.lua`:
    use {
    -- 'nvim-treesitter/nvim-treesitter', -- already required by ./syntax.lua
    'minijackson/epics.nvim',
    }

    -- =====
    -- TODO:
    -- =====
    -- check runtimepath with `:set rtp`, and make sure there is no other package-manager
    -- left-overs remaining (e.g. in `~/.config/nvim/pack/*/start/*`)

    -- =======
    -- TO ADD:
    -- =======
    -- https://github.com/nvim-treesitter/nvim-treesitter
    -- https://github.com/nvim-treesitter/nvim-treesitter/wiki/Extra-modules-and-plugins
    -- https://github.com/nvim-telescope/telescope.nvim
    -- https://github.com/kyazdani42/nvim-tree.lua

    -- =========
    -- TO CHECK:
    -- =========
    -- ==> https://github.com/rockerBOO/awesome-neovim <==
    -- https://github.com/akinsho/toggleterm.nvim
    -- https://github.com/yamatsum/nvim-cursorline
    -- https://github.com/lewis6991/impatient.nvim
    -- https://github.com/nvim-treesitter/nvim-treesitter
    -- https://github.com/dstein64/vim-startuptime

    -- run `sync()` if `packer.nvim` has just been automatically installed
    if packer_bootstrap then
      require('packer').sync()
    end

end)
