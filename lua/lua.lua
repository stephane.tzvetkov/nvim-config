-- lua
------

---------------------------------------------------------------------------------------------------
-- lsp sumneko lua language server
----------------------------------

-- ⚠️ see ./lsp.lua

-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#sumneko_lua
-- https://github.com/sumneko/lua-language-server
-- https://github.com/sumneko/lua-language-server/wiki
-- https://github.com/sumneko/lua-language-server/wiki/Build-and-Run

-- ⚠️ Prerequisites⚠️ :
-- * [ninja](https://repology.org/project/ninja/versions)
-- * g++
-- * the following installation procedure
--   (see https://github.com/sumneko/lua-language-server/wiki/Build-and-Run), e.g. like so:

-- ```console
--     $ mkdir -p $HOME/.local/share/nvim/site/lsp/nvim-lspconfig/
--     $ cd $HOME/.local/share/nvim/site/lsp/nvim-lspconfig/
--     $ git clone --depth=1 https://github.com/sumneko/lua-language-server
--     $ cd lua-language-server
--     $ git submodule update --depth 1 --init --recursive
--     $ cd 3rd/luamake
--     $ ./compile/install.sh
--     $ cd ../..
--     $ ./3rd/luamake/luamake rebuild
-- ```
-- * the following alias in your .bashrc (or .zshrc or whatever):
--   `alias luamake="$HOME/.local/share/nvim/site/lsp/nvim-lspconfig/lua-language-server/3rd/luamake/luamake"`
-- * don't forget to source your .bashrc (or .zshrc or whatever)

-- set the path to the sumneko's lua-language-server installation
local sumneko_root_path = vim.fn.stdpath('data') .. '/site/lsp/nvim-lspconfig/lua-language-server'

--local system_name
--if vim.fn.has("mac") == 1 then
--  system_name = "macOS"
--elseif vim.fn.has("unix") == 1 then
--  system_name = "Linux"
--elseif vim.fn.has('win32') == 1 then
--  system_name = "Windows"
--else
--  print("Unsupported system for sumneko")
--end
--local sumneko_binary = sumneko_root_path.."/bin/" .. system_name .. "/lua-language-server"
local sumneko_binary = sumneko_root_path.."/bin/lua-language-server"

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require'lspconfig'.sumneko_lua.setup {
    cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"};
    -- filetypes = { "lua" }; -- by default
    -- log_level = 2; -- by default
    -- root_dir = root_pattern(".git") or bufdir; -- by default
    settings = {
        Lua = {
            runtime = {
                -- tell the lsp which version of Lua you're using (most likely LuaJIT if nvim)
                version = 'LuaJIT',
                -- setup your lua path
                path = runtime_path,
            },
            diagnostics = {
                -- get the language server to recognize the `vim` global
                globals = {'vim'},
            },
            workspace = {
                -- make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
            },
            -- do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
        },
    },
}
