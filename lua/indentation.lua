-- indentation
--------------

-- see
-- https://github.com/rockerBOO/awesome-neovim#indent

---------------------------------------------------------------------------------------------------
-- indent
---------

vim.cmd [[
    filetype plugin indent on " see https://vi.stackexchange.com/a/10125
    syntax enable             " see https://stackoverflow.com/a/33380495
]]

local set = vim.opt

---------------------------------------------------------------------------------------------------
-- auto indentation and smart indentation
-----------------------------------------

set.autoindent = true  -- copy indent from current line when starting a new line
set.smartindent = true -- do smart autoindenting when starting a new line depending on the context

---------------------------------------------------------------------------------------------------
-- tab as 4 spaces
------------------
-- see https://stackoverflow.com/questions/1878974/redefine-tab-as-4-spaces

set.tabstop = 4      -- the (maximum) width, in spaces, of an actual tab character
set.softtabstop = 0  -- default 0: no mix of spaces an tabs is used when inserting tabs
set.expandtab = true -- use the appropriate number of spaces to insert a tab in insert mode
set.shiftwidth = 4   -- the width, in spaces, of an indent
set.smarttab = true  -- when on, a tab in front of a line inserts spaces (or tabs)
                     -- according to above opts

-- Note: to insert a real <Tab> when 'expandtab' is on, use CTRL-V<Tab>
-- Note: to replace all pre-existing real <Tab> with white-spaces using the new tabstop, use :retab

set.pastetoggle = '<F12>' -- in insert mode, paste from the clippoard with a good indentation.

---------------------------------------------------------------------------------------------------
-- print spaces and tabulations
-------------------------------

--vim.o.list = true -- print invisible chars from below listchars
--vim.o.listchars = 'tab:► ,space:·,extends:»,precedes:«' -- see :h listchars
---- space: U+00B7 (middle dot)
---- tab:   U+25BA (black right-pointing pointer)

