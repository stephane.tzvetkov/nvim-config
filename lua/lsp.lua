-- lsp
------

-- [nvim-lspconfig project](https://github.com/neovim/nvim-lspconfig)

-- [wiki](https://github.com/neovim/nvim-lspconfig/wiki)

-- [server-config](https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md)

-- see
-- https://github.com/rockerBOO/awesome-neovim#lsp

---------------------------------------------------------------------------------------------------
-- tips
-------

-- :LspInfo -> Shows the status of active and configured language servers.

-- The following support tab-completion for all arguments:
--
-- :LspStart <config_name> -> Start the requested server name. Will only successfully start if the
--                            command detects a root directory matching the current config. Pass
--                            autostart = false to your .setup{} call for a language server if you
--                            would like to launch clients solely with this command. Defaults to
--                            all servers matching current buffer filetype.
--
-- :LspStop <client_id>    -> Defaults to stopping all buffer clients.
--
-- :LspRestart <client_id> -> Defaults to restarting all buffer clients.


---------------------------------------------------------------------------------------------------
-- keybindings and completion
-----------------------------

local nvim_lsp = require('lspconfig')

-- use on_attach funct to only map the following keys after the lsp attaches to the current buffer
local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    -- enable completion triggered by <c-x><c-o>
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- mappings
    local opts = { unique=true, noremap=true, silent=true }

    -- see `:help vim.lsp.*` for documentation on any of the below functions
    --buf_set_keymap('i', '<c-space>', '<cmd>lua vim.lsp.buf.completion()<CR>', opts) --TODO
    --buf_set_keymap('i', '<c-;>', '<cmd>lua vim.lsp.buf.completion()<CR>', opts) --TODO
    buf_set_keymap('n', '!cac',  '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', '!rcac', '<cmd>lua vim.lsp.buf.range_code_action()<CR>', opts)
    buf_set_keymap('n', '!hov',  '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', '!for',  '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
    buf_set_keymap('n', '!rfor', '<cmd>lua vim.lsp.buf.range_formatting()<CR>', opts)
    buf_set_keymap('n', '!ref',  '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '!cref', '<cmd>lua vim.lsp.buf.clear_references()<CR>', opts)
    buf_set_keymap('n', '!ren',  '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '!def',  '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', '!tdef', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '!dec',  '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', '!imp',  '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', '!sig',  '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)

    buf_set_keymap('n', '!clea', '<cmd>lua vim.lsp.diagnostic.clear()<CR>', opts)
    buf_set_keymap('n', '!get', '<cmd>lua vim.lsp.diagnostic.get()<CR>', opts)
    buf_set_keymap('n', '!geta', '<cmd>lua vim.lsp.diagnostic.get_all()<CR>', opts)
    --buf_set_keymap('n', '!gld',  '<cmd>lua vim.lsp.diagnostic.get_line_diagnostics()<CR>', opts)
    buf_set_keymap('n', '!sld',  '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
    buf_set_keymap('n', '!gpd',  '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
    buf_set_keymap('n', '!gnd',  '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
    buf_set_keymap('n', '!slo',  '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)

    buf_set_keymap('n', '!wsym', '<cmd>lua vim.lsp.buf.workspace_symbol()<CR>', opts)
    buf_set_keymap('n', '!ica',  '<cmd>lua vim.lsp.buf.incoming_calls()<CR>', opts)
    buf_set_keymap('n', '!oca',  '<cmd>lua vim.lsp.buf.outgoing_calls()<CR>', opts)
    buf_set_keymap('n', '!exe',  '<cmd>lua vim.lsp.buf.execute_command()<CR>', opts)
    buf_set_keymap('n', '!rea',  '<cmd>lua vim.lsp.buf.server_ready()<CR>', opts)
    buf_set_keymap('n', '!awf',  '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '!rwf',  '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '!lwf',  '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
end

-- use a loop to call 'setup' on multiple lsps and map buffer local keybindings when lsp attaches
vim.g.lsp_servers = {
                     'sumneko_lua', -- ⚠️ see ./lua.lua
                     'rust_analyzer',
                     'pyright', -- ⚠️ see ./python.lua
                     'tsserver'
                    }
--local servers = { 'sumneko_lua', 'rust_analyzer', 'pyright', 'tsserver' }
for _, lsp in ipairs(vim.g.lsp_servers) do
    nvim_lsp[lsp].setup {
        on_attach = on_attach,
        flags = {
            debounce_text_changes = 150,
        }
    }
end

---------------------------------------------------------------------------------------------------
-- lsp debug
------------

-- see minimal config for debugging:
-- https://github.com/neovim/nvim-lspconfig/blob/master/test/minimal_init.lua

-- see logs in `$HOME/.cache/nvim/lsp.log`
-- :help lsp-log

--vim.lsp.set_log_level('debug')
--vim.lsp.set_log_level('trace')
--if vim.fn.has 'nvim-0.5.1' == 1 then
--    require('vim.lsp.log').set_format_func(vim.inspect)
--end

-- check LSP FAQ:
-- :help lsp-faq
--
-- check that an LSP client has attached to the current buffer:
-- :lua print(vim.inspect(vim.lsp.buf_get_clients()))

-- shows the status of active and configured language servers:
-- :LspInfo
