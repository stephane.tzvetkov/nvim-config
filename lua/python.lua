-- python
---------

---------------------------------------------------------------------------------------------------
-- lsp with pyright
-------------------

-- ⚠️ see ./lsp.lua

-- https://github.com/Microsoft/pyright
-- https://github.com/microsoft/pyright/tree/main/docs
-- https://github.com/microsoft/pyright/blob/main/docs/getting-started.md
-- https://github.com/microsoft/pyright/blob/main/docs/command-line.md

-- ⚠️ Prerequisites⚠️ :
-- * you just have to install [pyright](https://repology.org/project/pyright/versions)
