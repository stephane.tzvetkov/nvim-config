-- keys and commands mapping
----------------------------

local map = vim.api.nvim_set_keymap

---------------------------------------------------------------------------------------------------
-- config
---------

vim.opt.timeoutlen = 1000 -- see https://neovim.io/doc/user/options.html#'timeoutlen'

---------------------------------------------------------------------------------------------------
-- leaders
----------

vim.g.mapleader = ';'
vim.g.maplocalleader=","

---------------------------------------------------------------------------------------------------
-- registers
------------

-- Don't loose default register when pasting?
-- see
-- https://superuser.com/questions/321547/how-do-i-replace-paste-yanked-text-in-vim-without-yanking-the-deleted-lines

--map('v', 'p', '0p', {unique=true, noremap=true})
--map('v', 'P', '0P', {unique=true, noremap=true})

---------------------------------------------------------------------------------------------------
-- buffers, windows and tab pages
---------------------------------

-- see https://vim.fandom.com/wiki/Switch_between_Vim_window_splits_easily
-- see https://stackoverflow.com/questions/327415/can-windows-key-be-mapped-in-vim#22938137
-- see diff between buffers, windows and tabs (TODO):
-- https://stackoverflow.com/questions/26708822/why-do-vim-experts-prefer-buffers-over-tabs#26710166

-- buffers
----------
-- see :h buffers

-- go to previous and next buffer:
map('n', 'J', ':bp!<CR>', {unique=true, noremap=true})
map('n', 'K', ':bn!<CR>', {unique=true, noremap=true})
-- close current buffer:
map('n', 'X', ':bd!<CR>', {unique=true, noremap=true})

-- windows
----------
-- see :h CTRL-W
-- see :h windows

-- go to up, down, left or right window:
--map('n', 'C-Up', ':wincmd k<CR>', {unique=true, noremap=true})      -- or CTRL-W k
--map('n', 'C-Down', ':wincmd j<CR>', {unique=true, noremap=true})    -- or CTRL-W j
--map('n', 'C-Left', ':wincmd h<CR>', {unique=true, noremap=true})    -- or CTRL-W h
--map('n', 'C-Right', ':wincmd l<CR>', {unique=true, noremap=true})   -- or CTRL-W l

-- go to next, previous window:
--map('n', 'C-j', ':wincmd w<CR>', {unique=true, noremap=true}) -- or CTRL-W w
--map('n', 'C-n', ':wincmd W<CR>', {unique=true, noremap=true}) -- or CTRL-W W

-- open new window horizontally (duplicating current one):
--map('n', 'C-h', ':wincmd s<CR>', {unique=true, noremap=true})  -- or :split
-- open new window vertically (duplicating current one):
--map('n', 'C-v', ':wincmd v<CR>', {unique=true, noremap=true})  -- or :vsplit

-- open new window vertically (creating new file):
--map('n', 'C-hn', ':new<CR>', {unique=true, noremap=true})      -- or :new <filename>
-- open new window vertically (creating new file):
--map('n', 'C-vn', ':vnew<CR>', {unique=true, noremap=true})     -- or :vnew <filename>

-- close current window:
--map('n', 'C-x', ':wincmd S<CR>', {unique=true, noremap=true})  -- or CTRL-W q (or just :q)
-- close any other other window than the current one:
--map('n', 'C-o', ':wincmd S<CR>', {unique=true, noremap=true})  -- or CTRL-W o (or :only)

-- make all windows (almost) equally high and wide:
--map('n', 'C-=', '<CTRL_W_=>', {unique=true, noremap=true})      -- or CTRL-W =
-- decrease current window size:
--map('n', 'C--', ':resize -N<CR>', {unique=true, noremap=true})  -- or CTRL-W -
-- increase current window size:
--map('n', 'C-+', ':resize +N<CR>', {unique=true, noremap=true})  -- or CTRL-W +

-- tab pages
------------
-- see :h CTRL-W
-- see :h tabs

-- go to previous and next tab page:
--map('n', 'C-S-j', 'gT', {unique=true, noremap=true})   -- or gT
--map('n', 'C-S-k', 'gt', {unique=true, noremap=true})   -- or gt

-- move current window to the next tab page:
--map('n', 'C-S-j', '<CTRL_W_T>', {unique=true, noremap=true})   -- or CTRL-W T

-- ------------------------------------------------------------------------------------------------
-- wrap moving
--------------

-- TODO
-- see https://vim.fandom.com/wiki/Move_cursor_by_display_lines_when_wrapping

--function ToggleWrap()
--  if &wrap
--    echo "Wrap OFF"
--    setlocal nowrap
--    set virtualedit=all
--    silent! nunmap <buffer> <Up>
--    silent! nunmap <buffer> <Down>
--    silent! nunmap <buffer> <Home>
--    silent! nunmap <buffer> <End>
--    silent! iunmap <buffer> <Up>
--    silent! iunmap <buffer> <Down>
--    silent! iunmap <buffer> <Home>
--    silent! iunmap <buffer> <End>
--  else
--    echo "Wrap ON"
--    setlocal wrap linebreak nolist
--    set virtualedit=
--    setlocal display+=lastline
--    noremap  <buffer> <silent> <Up>   gk
--    noremap  <buffer> <silent> <Down> gj
--    noremap  <buffer> <silent> <Home> g<Home>
--    noremap  <buffer> <silent> <End>  g<End>
--    inoremap <buffer> <silent> <Up>   <C-o>gk
--    inoremap <buffer> <silent> <Down> <C-o>gj
--    inoremap <buffer> <silent> <Home> <C-o>g<Home>
--    inoremap <buffer> <silent> <End>  <C-o>g<End>
--  endif
--endfunction
--
--noremap <silent> <Leader>w :call ToggleWrap()<CR>

---------------------------------------------------------------------------------------------------
-- nvim-cmp mapping setup
-------------------------
--
-- see https://github.com/hrsh7th/nvim-cmp#setup
-- see https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings#luasnip

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local luasnip = require("luasnip")
local cmp = require'cmp'
cmp.setup({
    mapping = {
        ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
        ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<C-y>'] = cmp.config.disable, -- `disable` if you want to remove the default <C-y> mapping
        ['<C-e>'] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        }),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
              luasnip.expand_or_jump()
            elseif has_words_before() then
              cmp.complete()
            else
              fallback()
            end
        end, { "i", "s" }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
        end, { "i", "s" }),
    },
    -- see the rest of the nvim-cmp setup in ./completion.lua
})

-- ------------------------------------------------------------------------------------------------
-- misc
-------

-- remove (/strip) any trailling spaces in the current buffer, and replace all sequences of
-- white-space containing a <Tab> with tabstop (see `./indentation.lua`):
function _G.strip_trailing_whitespaces_and_retab()
    local row,col= unpack(vim.api.nvim_win_get_cursor(0))
    vim.api.nvim_command([[%s/\s\+$//e]])
    vim.api.nvim_win_set_cursor(0, { row, col })
    vim.api.nvim_command('retab')
end
map('n', '<Leader><Space>', ':call v:lua.strip_trailing_whitespaces_and_retab()<CR>', {unique=true, noremap=true})

-- restrict current line width, by formatting it - if necessary - into a paragraph , in order to
-- avoid line wrapping:
function _G.restrict_line_width()
    vim.api.nvim_command('set textwidth=99')
    -- format ('gw') current line into paragraph ('ap'):
    vim.api.nvim_command('execute "normal gwap"')
    vim.api.nvim_command('set textwidth=0')
end
map('n', '<LocalLeader>!', ':call v:lua.restrict_line_width()<CR>', {unique=true, noremap=true})

-- restrict all the lines width of the current selection, by formatting them - if necessary - into
-- paragraphs, in order to avoid line wrapping:
-- TODO?

---- restrict all the lines width of the current buffer, by formatting them - if necessary - into
---- paragraphs, in order to avoid line wrapping:
--function _G.restrict_file_width()
--    vim.api.nvim_command('set textwidth=99')
--    local row,col= unpack(vim.api.nvim_win_get_cursor(0))
--    -- format ('gw') file width from the beggening ('gg') to the end ('G') of the file:
--    vim.api.nvim_command('execute "normal gggwG"')
--    vim.api.nvim_win_set_cursor(0, { row, col })
--    vim.api.nvim_command('set textwidth=0')
--end
--map('n', '<Leader>!', ':call v:lua.restrict_file_width()<CR>', {unique=true, noremap=true})

-- insert a back tild:
map('n', "<LocalLeader>'", ':normal a`<ESC>', {unique=true, noremap=true})

-- map ctrl-a to append at the end of a word (shift-a already append at the end of a line):
map('n', '<C-a>', 'ea', {unique=true, noremap=true})

-- search and replace <word> under cursor:
map('n', '<LocalLeader>s', [[:%s/\<<C-r><C-w>\>//gc<Left><Left><Left>]], {unique=true, noremap=true})

-- search and replace *word* under cursor
map('n', '<Leader>s', [[:%s/<C-r><C-w>//gc<Left><Left><Left>]], {unique=true, noremap=true})

