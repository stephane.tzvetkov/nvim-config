-- spell checker
----------------

-- see https://github.com/rockerBOO/awesome-neovim#spellcheck
-- see https://github.com/lewis6991/spellsitter.nvim
-- see https://stackoverflow.com/questions/640351/how-can-i-spellcheck-in-gvim

require('spellsitter').setup()

vim.opt.spelllang='en_us,en_gb,fr'
vim.opt.spell=false

-- from `:help spell`
--
-- - in normal mode:
--     ]s          move to next misspelled word after the cursor.
--
--     [s          move the previous misspelled word before the cursor.
--
--     z=          print and select correct suggestions for the misspelled word under/after the cursor
--
--     1z=         select first suggestion directly for the misspelled word under/after the cursor
--
--     zg          add the word under the cursor as a good word
--
--     zw          mark the word under the cursor as a wrong (bad) word
--
--     zug         undo `zw` and `zg` for the word under cursor
--     zuw         undo `zw` and `zg` for the word under cursor
--
-- - in insert mode:
--     CTRL-X s    print suggestions
--     CTRL-N      navigate to the next suggestion
--     TAB         navigate to the next suggestion
--     CTRL-P      navigate to the previous suggestion
--     Shift-TAB   navigate to the previous suggestion
