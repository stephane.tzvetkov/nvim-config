
require("epics").setup {
    -- Use nvim-treesitter's ensure_install to ensure you have all the required
	-- tree-sitter grammars
	ensure_ts_installed = true,
}
