-- git
------

-- ------------------------------------------------------------------------------------------------
-- git plugins
-- -----------

-- * see `?` in `./plugins.lua` for
--   [lualine](https://github.com/nvim-lualine/lualine.nvim) and the below statusline and tabline
--   section

-- * see https://github.com/rockerBOO/awesome-neovim#git

--------------------------------------------------------------------------------------------------
-- gitgutters colors
--------------------

-- see last section ("Gutter" section) of ./theme.vim

