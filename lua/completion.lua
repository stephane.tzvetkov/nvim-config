-- completion
-------------

-- see nvim-lspconfig recommandation:
-- https://github.com/hrsh7th/nvim-cmp/
-- https://github.com/neovim/nvim-lspconfig/wiki/Autocompletion
--
-- see:
-- https://github.com/rockerBOO/awesome-neovim#completion
-- https://github.com/folke/which-key.nvim
-- https://github.com/hrsh7th/nvim-compe
-- https://github.com/hrsh7th/nvim-compe/blob/master/doc/compe.txt


---------------------------------------------------------------------------------------------------
-- nvim-cmp setup
-----------------
--
-- see https://github.com/hrsh7th/nvim-cmp#setup
-- see https://github.com/hrsh7th/nvim-cmp/wiki/Example-mappings#luasnip

vim.cmd('set completeopt=menu,menuone,noselect')

local cmp = require'cmp'
cmp.setup({
    -- snippet engine (REQUIRED)
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
    -- mapping (REQUIRED)
        -- see `mapping = {...}` in ./mapping.lua
    -- sources (REQUIRED)
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
    }, {
        { name = 'buffer' },   -- https://github.com/hrsh7th/cmp-buffer
        { name = 'nvim_lua' }, -- https://github.com/hrsh7th/cmp-nvim-lua
    })
})

-- use buffer source for `/` (this won't work anymore if `native_menu` is enabled).
cmp.setup.cmdline('/', {
    sources = {
        { name = 'buffer' }
    }
})

-- use cmdline & path source for ':' (this won't work anymore if `native_menu` is enabled).
cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

-- lspconfig capabilities
local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
local nvim_lsp = require('lspconfig')
for _, lsp in ipairs(vim.g.lsp_servers) do
    nvim_lsp[lsp].setup {
        capabilities = capabilities
    }
end
