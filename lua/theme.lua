-- theme
--------

-- see
-- https://github.com/rockerBOO/awesome-neovim#colorscheme
-- https://github.com/rockerBOO/awesome-neovim#colorscheme-creation
-- https://github.com/rockerBOO/awesome-neovim#color
-- https://github.com/rockerBOO/awesome-neovim#tabline
-- https://github.com/rockerBOO/awesome-neovim#statusline

---------------------------------------------------------------------------------------------------
-- color scheme
---------------
-- [lualine](https://github.com/nvim-lualine/lualine.nvim)
-- [material](https://github.com/marko-cerovac/material.nvim)

-- some of my favorite x11 colors (see https://en.wikipedia.org/wiki/X11_color_names)
local colors = {
    white     = '#f8f8ff', -- x11 ghost white
    gray      = '#bebebe', -- x11 gray
    black     = '#000000', -- x11 black
    red       = '#ee0000', -- x11 red variant 2
    orange    = '#ee9a00', -- x11 orange variant 2
    yellow    = '#eeee00', -- x11 yellow variant 2
    green     = '#00ee00', -- x11 green variant 2
    cyan      = '#00eeee', -- x11 cyan variant 2
    paleblue  = '#aeeeee', -- x11 pale turquoise variant 2
    royalblue = '#436eee', -- x11 royal blue variant 2
    blue      = '#0000ee', -- x11 blue variant 2
    purple    = '#912cee', -- x11 purple variant 2
  --pink      = '#eea9b8', -- x11 pink variant 2
    pink      = '#ff1493', -- x11 deep pink
    silver    = '#c0c0c0', -- x11 silver
    darkgray  = '#404040', -- x11 dark gray
    disabled  = '#404040',
  --disabled  = '#464B5D',

  --darkred    = '#dc6068',
  --darkgreen  = '#abcf76',
  --darkyellow = '#e6b455',
  --darkblue   = '#6e98eb',
  --darkcyan   = '#71c6e7',
  --darkpurple = '#b480d6',
  --darkorange = '#e2795b',

  --error  = '#FF5370',
  --link   = '#80CBC4',
  --cursor = '#FFCC00',
}

vim.opt.termguicolors = true -- see https://neovim.io/doc/user/options.html#'termguicolors

require('colorizer').setup()
require('material').setup({
    -- see all *.lua files from https://github.com/marko-cerovac/material.nvim/tree/main/lua/material

    disable = {
        background = true, -- prevent theme from setting the background
        term_colors = true, -- prevent theme from setting terminal colors
        eob_lines = false -- hide the end-of-buffer lines
    },

    custom_highlights = {}, -- overwrite highlights with your own

    -- x11 theming (see https://en.wikipedia.org/wiki/X11_color_names)
    custom_colors = { -- overwrite any color with your own
        white    = colors.white,
        gray     = colors.gray,
        black    = colors.black,
        red      = colors.red,
        green    = colors.green,
        yellow   = colors.yellow,
        blue     = colors.royalblue,
        paleblue = colors.paleblue,
        cyan     = colors.cyan,
        purple   = colors.purple,
        orange   = colors.orange,
        pink     = colors.pink,

        -- deep ocean theme style
        --bg           = '#0F111A',
        --bg_alt       = '#090B10',
        --fg           = '#A6ACCD',
        fg           = colors.white,
        --text         = '#717CB4',
        --comments     = '#464B5D',
        --selection    = '#1F2233',
        --contrast     = '#090B10',
        --active       = '#1A1C25',
        --border       = '#1F2233',
        --line_numbers = '#3B3F51',
        --highlight    = '#1F2233',
        --disabled     = '#464B5D',
        --accent       = '#84FFFF',

    } -- see https://github.com/marko-cerovac/material.nvim/blob/main/lua/material/colors.lua
})

vim.cmd 'colorscheme material'
--vim.cmd('colorscheme material')
vim.g.material_style = 'deep ocean'
--vim.g.material_style = "deep ocean"

---------------------------------------------------------------------------------------------------
-- status line and tab line
---------------------------
-- [lualine](https://github.com/nvim-lualine/lualine.nvim)

-- alternatives:
-- [statusline](https://github.com/rockerBOO/awesome-neovim#statusline)
-- [tabline](https://github.com/rockerBOO/awesome-neovim#tabline)
-- [galaxyline](https://github.com/glepnir/galaxyline.nvim)
-- [express_line](https://github.com/tjdevries/express_line.nvim)
-- [neoline](https://github.com/adelarsq/neoline.vim)
-- [hardline](https://github.com/ojroques/nvim-hardline)
-- [bubbly](https://github.com/datwaft/bubbly.nvim)
-- [statusline](https://github.com/beauwilliams/statusline.lua)
-- [staline](https://github.com/tamton-aquib/staline.nvim)
-- [feline](https://github.com/Famiu/feline.nvim)
-- [windline](https://github.com/windwp/windline.nvim)
-- [vacuumline](https://github.com/konapun/vacuumline.nvim)

--local material_nvim = require'lualine.themes.16color'
--material_nvim.normal.c.bg = '#112233'


-- personnal lualine theme:
-- e.g. see https://github.com/nvim-lualine/lualine.nvim/blob/master/lua/lualine/themes/ayu_dark.lua
local x11_lualine_theme= {
    normal = {
        a = {fg = colors.black, bg = colors.royalblue, gui = 'bold'},
        b = {fg = colors.white},
        c = {fg = colors.white},
    },
    insert = {
        a = {fg = colors.black, bg = colors.green, gui = 'bold'},
        b = {fg = colors.white},
    },
    visual = {
        a = {fg = colors.black, bg = colors.purple, gui = 'bold'},
        b = {fg = colors.white},
    },
    replace = {
        a = {fg = colors.black, bg = colors.red, gui = 'bold'},
        b = {fg = colors.white},
    },
    command = {
        a = {fg = colors.black, bg = colors.yellow, gui = 'bold'},
        b = {fg = colors.white},
    },
    inactive = {
        a = {fg = colors.disabled, bg = colors.black, gui = 'bold'},
        --a = {fg = colors.disabled, gui = 'bold'},
        b = {fg = colors.disabled},
        c = {fg = colors.disabled}
    }
}

-- lualine setup:
require'lualine'.setup {
  options = {
    icons_enabled = true,
    theme = x11_lualine_theme,
    --theme = 'material-nvim',
    --theme = '16color',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', {'diagnostics', sources={'nvim_lsp'}}},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {
    lualine_a = {'buffers'},
    lualine_b = {},
    lualine_c = {},
    lualine_x = {},
    lualine_y = {},
    lualine_z = {'tabs'}
  },
  extensions = {}
}

-- make sure lualine will redraw the tab line at startup:
vim.cmd([[
augroup line_return
    au!
    au VimEnter * lua vim.defer_fn(function() vim.api.nvim_command('redrawtabline') end, 0)
augroup END
]])

---------------------------------------------------------------------------------------------------
-- print spaces and tabulations
-------------------------------

-- see ./indentation.lua "print spaces and tabulations" section

