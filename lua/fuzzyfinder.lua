-- fuzzy finder
---------------

-- [telescope](https://github.com/nvim-telescope/telescope.nvim)

-- ⚠️ [ripgrep](https://github.com/BurntSushi/ripgrep) is required for live_grep and grep_string
-- functionalities of telescope!⚠️

-- Optional dependencies
------------------------
-- sharkdp/fd (finder)
-- nvim-treesitter/nvim-treesitter (finder/preview)
-- neovim LSP (picker)
-- devicons (icons)


nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>
