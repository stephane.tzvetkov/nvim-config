-- syntax
---------

-- see
-- https://github.com/rockerBOO/awesome-neovim#syntax
-- https://github.com/rockerBOO/awesome-neovim#cursorline

-- https://github.com/RRethy/vim-illuminate

--require'lspconfig'.gopls.setup {
--  on_attach = function(client)
--    -- [[ other on_attach code ]]
--    require 'illuminate'.on_attach(client)
--  end,
--}

-- vim.api.nvim_command [[ hi def link LspReferenceText CursorLine ]]
-- vim.api.nvim_command [[ hi def link LspReferenceWrite CursorLine ]]
-- vim.api.nvim_command [[ hi def link LspReferenceRead CursorLine ]]

-- vim.api.nvim_set_keymap('n', '<LocalLeader-n>', '<cmd>lua require"illuminate".next_reference{wrap=true}<cr>', {unique=true, noremap=true})
-- vim.api.nvim_set_keymap('n', '<LocalLeader-m>', '<cmd>lua require"illuminate".next_reference{reverse=true,wrap=true}<cr>', {unique=true, noremap=true})

-- vim.g.Illuminate_delay = 0
-- vim.g.Illuminate_highlightUnderCursor = 0

--FIXME: running `:hi link illuminatedWord Visual` will work, but not the previous config file

require'nvim-treesitter.configs'.setup {
  ensure_installed = { "bash", "c", "c_sharp", "clojure", "cmake", "cpp", "css", "dart", "diff", "dockerfile", "elixir", "erlang", "fortran", "gdscript", "go", "haskell", "help", "html", "http", "java", "javascript", "json", "julia", "kotlin", "latex", "lua", "make", "meson", "ninja", "nix", "ocaml", "pascal", "perl", "php", "python", "r", "ruby", "rust", "scala", "sparql", "sql", "swift", "toml", "typescript", "vala", "vim", "yaml", "zig" }, -- markdown ?
  highlight = {
    enable = true, -- `false` will disable the whole extension
  },
}
